# Lernjournal m231


## Branches in Git

Jeder Branch ist wie ein individueller Skript, an dem unterschiedliche Entwickler gleichzeitig arbeiten können, ohne sich gegenseitig zu stören. Ein anderer Entwickler kann seine Ideen ausprobieren und Änderungen vornehmen, die vorerst nur in seinem eigenen Branch sichtbar sind. Um die Änderungen im eigenen Branch in das Hauptskript einzufügen, stellen wir eine Verbindung her. Die Branch müsste man im Hauptskript verlinken. Hier ein Beispiel zur Verlinkung: [Link zu Branches](branches.md)  
branches.md entspricht den Datei Namen.

<img src="https://th.bing.com/th/id/OIP.2N2fOjoSdTVvnhQosUTpnwHaG2?rs=1&pid=ImgDetMain" alt="Bildbeschreibung" width="230" height="200">
